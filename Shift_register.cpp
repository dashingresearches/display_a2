#include "Shift_register.h"
         

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given
*/

////////////////////////////////////////////////////////////////////////////////
tShift_register::tShift_register(int Size):Data(Size)
{
  for(int i = 0; i < Size; i++) Data[i] = 0;
};


////////////////////////////////////////////////////////////////////////////////
void tShift_register::Shift(int Start, int End, int Amount)
{

  if(Amount < 0)
  {

    while(Amount)
    {

      bool Carry = false;

      for(int i = End; i-- != Start ;)
      {

        bool Next_carry;

        if(Data[i] & 0x80)
          Next_carry = true;
        else
          Next_carry = false;

        Data[i] = (Data[i] << 1) | Carry;

        Carry = Next_carry;

      }

      Amount++;

    }//while(Amount)

  }//if(Amount < 0)
  else
  {

    while(Amount)
    {

      u1x Carry = 0x0;

      for(int i = Start; i < End ; i++)
      {

        u1x Next_carry = 0;

        if(Data[i] & 0x1)
          Next_carry = 0x80;
        else
          Next_carry = 0x0;

        Data[i] = (Data[i] >> 1) | Carry;

        Carry = Next_carry;

      }

      Amount--;

    }//while(Amount)

  }//else //if(Amount < 0)

}//void tShift_register::Shift(u2x Amount)


////////////////////////////////////////////////////////////////////////////////
void tShift_register::Clear()
{

  for(int i = 0; i < Data.size(); i++)
  {

    Data[i] = 0;

  }

}


////////////////////////////////////////////////////////////////////////////////
void tShift_register::Load(int Start_index_in_destination, const u1x * Source, int Width)
{

  if(Data.size() < (Start_index_in_destination + Width) )
  {

    Data.resize(Start_index_in_destination + Width, 0);

  }

  while(Width--)
  {

    Data[Start_index_in_destination + Width] = Source[Width];

  }

}

////////////////////////////////////////////////////////////////////////////////
void tShift_register::Load( u1x * Destination, int Start_index_in_source, int Width)
{

  if(Data.size() < (Start_index_in_source + Width) )
  {

    return;

  }

  while(Width--)
  {

    Destination[Width] = Data[Start_index_in_source + Width];

  }

}


////////////////////////////////////////////////////////////////////////////////
void tShift_register::Or(int Start_index_in_destination, u1x * Source, int Amount_of_bytes)
{

  if(Data.size() < (Start_index_in_destination + Amount_of_bytes))
  {

    Data.resize((Start_index_in_destination + Amount_of_bytes), 0);

  }

  while(Amount_of_bytes--)
  {

    Data[Start_index_in_destination + Amount_of_bytes] |= Source[Amount_of_bytes];

  }

}

////////////////////////////////////////////////////////////////////////////////
void tShift_register::Or(u1x * Destination, int Start_index_in_source, int Width)
{

  if(Data.size() < (Start_index_in_source + Width) )
  {

    return;

  }

  while(Width--)
  {

    Destination[Width] |= Data[Start_index_in_source + Width];

  }

}


////////////////////////////////////////////////////////////////////////////////
void tShift_register::Or(int Start_index_in_destination, tShift_register &Source, int Amount_of_bytes)
{

  if(Data.size() < (Start_index_in_destination + Amount_of_bytes))
  {

    Data.resize((Start_index_in_destination + Amount_of_bytes), 0);

  }

  while(Amount_of_bytes--)
  {

    Data[Start_index_in_destination + Amount_of_bytes] |= Source[Amount_of_bytes];

  }

}

////////////////////////////////////////////////////////////////////////////////
u1x& tShift_register::operator [] (u2x Index) {return Data[Index];}


////////////////////////////////////////////////////////////////////////////////
tShift_register& tShift_register::operator = (const tShift_register& SRC)
{

  Data.resize(SRC.Data.size());

  for(int i = 0; i < SRC.Data.size(); i++)
  {

    Data[i] = SRC.Data[i];

  }

}



