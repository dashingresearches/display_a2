#include "Display.h"

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/


const u1x tDisplay::Masks_array__left[8]=\
{0x00,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe};

const u1x tDisplay::Masks_array__right[8]=\
{0xff,0x7f,0x3f,0x1f,0x0f,0x07,0x03,0x01};

const u1x tDisplay::Masks_array__left_for_line_buffer[8]=\
{0xff,0x7f,0x3f,0x1f,0x0f,0x07,0x03,0x01};

const u1x tDisplay::Masks_array__right_for_line_buffer[8]=\
{0x00,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe};




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay::Draw(TCanvas * Canvas)
{

  Canvas->Pen->Color = clBlue;
  Canvas->Pen->Width = 1;

  for(int y = 0; y < Line_buffer.size(); y ++)
  {

    for(int x = 0; x < Line_buffer[y].size(); x ++)
    {

      for(int b = 0, mask = 0x80; b < 8; b ++, mask = mask >> 1)
      {

        if(Line_buffer[y][x] & mask)

          Canvas->Brush->Color = clBlue;

        else

          Canvas->Brush->Color = clWhite;

        Canvas->Rectangle( (x*8 + b) * 4 - 1, y * 4 - 1, (x*8 + b + 1) * 4, (y + 1) * 4);

      }//for(int b = 0, mask = 1; b < 8; b ++, mask = mask << 1)


      Canvas->Pen->Color = clBlack;
      Canvas->Pen->Width = 2;

      Canvas->MoveTo((x*8) * mVideomemory_pixel_size - 1, 0);
      Canvas->LineTo((x*8) * mVideomemory_pixel_size - 1, Line_buffer.size()*mVideomemory_pixel_size - 1);

      Canvas->Pen->Color = clBlue;
      Canvas->Pen->Width = 1;

    }//for(int x = 0; x < Line_buffer[y].Size(); x ++)

  }//for(int y = 0; y < Line_buffer.size(); y ++)

}//void tDisplay::Draw(TCanvas * Canvas)

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay::Set_font(class tFont * argFont)
{

  Current_font = argFont;

}


