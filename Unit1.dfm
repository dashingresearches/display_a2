object Form1: TForm1
  Left = 29
  Top = 144
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'automatons.demo.1'
  ClientHeight = 728
  ClientWidth = 1243
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1243
    Height = 728
    ActivePage = TabSheet1
    Align = alClient
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1044#1080#1089#1087#1083#1077#1081
      object Image1: TImage
        Left = 8
        Top = 32
        Width = 1025
        Height = 257
      end
      object Label1: TLabel
        Left = 16
        Top = 626
        Width = 49
        Height = 20
        Caption = #1058#1077#1082#1089#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 11
        Top = 294
        Width = 19
        Height = 20
        Caption = 'x0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 11
        Top = 342
        Width = 19
        Height = 20
        Caption = 'x1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 11
        Top = 404
        Width = 19
        Height = 20
        Caption = 'y0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 11
        Top = 452
        Width = 19
        Height = 20
        Caption = 'y1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = -2
        Top = 514
        Width = 58
        Height = 20
        Caption = 'X_shift'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = -2
        Top = 562
        Width = 58
        Height = 20
        Caption = 'Y_shift'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 259
        Top = 6
        Width = 61
        Height = 20
        Caption = #1064#1088#1080#1092#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TrackBar1: TTrackBar
        Left = 88
        Top = 296
        Width = 955
        Height = 45
        Max = 256
        Min = -256
        Orientation = trHorizontal
        Frequency = 1
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 0
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar1Change
      end
      object TrackBar2: TTrackBar
        Left = 88
        Top = 405
        Width = 955
        Height = 45
        Max = 64
        Min = -64
        Orientation = trHorizontal
        Frequency = 1
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 1
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar2Change
      end
      object TrackBar3: TTrackBar
        Left = 88
        Top = 516
        Width = 955
        Height = 45
        Max = 256
        Min = -256
        Orientation = trHorizontal
        Frequency = 1
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 2
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar3Change
      end
      object Edit1: TEdit
        Left = 96
        Top = 624
        Width = 931
        Height = 28
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Text = 'a'
        OnKeyPress = Edit1KeyPress
      end
      object TrackBar4: TTrackBar
        Left = 88
        Top = 344
        Width = 955
        Height = 45
        Max = 256
        Min = -256
        Orientation = trHorizontal
        Frequency = 1
        Position = 200
        SelEnd = 0
        SelStart = 0
        TabOrder = 4
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar4Change
      end
      object TrackBar5: TTrackBar
        Left = 88
        Top = 454
        Width = 955
        Height = 45
        Max = 64
        Min = -64
        Orientation = trHorizontal
        Frequency = 1
        Position = 56
        SelEnd = 0
        SelStart = 0
        TabOrder = 5
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar5Change
      end
      object TrackBar6: TTrackBar
        Left = 88
        Top = 564
        Width = 955
        Height = 45
        Max = 50
        Min = -50
        Orientation = trHorizontal
        Frequency = 1
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 6
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar6Change
      end
      object Memo1: TMemo
        Left = 1050
        Top = 32
        Width = 185
        Height = 668
        Lines.Strings = (
          'Memo1')
        ScrollBars = ssVertical
        TabOrder = 7
      end
      object RadioButton1: TRadioButton
        Left = 24
        Top = 0
        Width = 113
        Height = 17
        Caption = #1042#1080#1076#1077#1086#1087#1072#1084#1103#1090#1100
        Checked = True
        TabOrder = 8
        TabStop = True
        OnClick = RadioButton1Click
      end
      object RadioButton2: TRadioButton
        Left = 128
        Top = 0
        Width = 113
        Height = 17
        Caption = #1057#1090#1088#1086#1095#1085#1099#1081' '#1073#1091#1092#1077#1088
        TabOrder = 9
        OnClick = RadioButton2Click
      end
      object ComboBox1: TComboBox
        Left = 328
        Top = 1
        Width = 145
        Height = 28
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 20
        ItemIndex = 0
        ParentFont = False
        TabOrder = 10
        Text = '6x8'
        OnChange = ComboBox1Change
        Items.Strings = (
          '6x8'
          '8x10'
          '8x16'
          '16x24')
      end
      object CheckBox1: TCheckBox
        Left = 1048
        Top = 8
        Width = 169
        Height = 17
        Caption = #1054#1090#1083#1072#1076#1086#1095#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
        TabOrder = 11
        OnClick = CheckBox1Click
      end
    end
  end
end
