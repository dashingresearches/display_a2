#ifndef Main_tab_included
#define Main_tab_included


/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/

// �������� ����� - ������� ��������������� �������� �������.
// ���������� �����������, �������� �����, � ������ ����������, �������
// ������������ �������� � ��.
#include <Classes.hpp>
#include <SyncObjs.hpp>

#include "Display.h"

#include "Unit1.h"

#include "Videomemory.h"

// ����� ��������� ����� ����� � ����� ����������.
class tMain_tab_process: public TThread
{

  public:

  int x0;
  int y0;

  int x1;
  int y1;

  int x_shift;
  int y_shift;

  int x;
  int y;

  tDisplay Display;

  using TThread::Synchronize;

  TEvent * Was_event;

  AnsiString Text;

  bool Let_debug_information;

  Graphics::TBitmap * Bitmap;

  /////////////////////////////////////////////////////
  tMain_tab_process()
  :TThread(false),
   Display(256,64,24)
  {

    Bitmap = new Graphics::TBitmap;
    Bitmap->Width = 256 * mVideomemory_pixel_size;
    Bitmap->Height = 64 * mVideomemory_pixel_size;

    Was_event = new TEvent(0, true, false, "Was_event" );
    //Was_event->ResetEvent();
    FreeOnTerminate = true;

  };

  /////////////////////////////////////////////////////
  virtual void __fastcall Execute()
  {

    while(!Terminated)
    {
            Was_event->WaitFor(INFINITE);

      Synchronize(SOut);

      Was_event->ResetEvent();


    }

  };


  /////////////////////////////////////////////////////
  void __fastcall SOut()
  {
     //       return;
    Display.Videomemory.Fill_1();

//    if(Let_debug_information)

   //   Display.Out_text(x0, y0, x1, y1, x_shift, y_shift, Text.c_str(), Form1->Memo1);

  //  else

      Display.Out_text(x0, y0, x1, y1, x_shift, y_shift, Text.c_str());



    if(Form1->RadioButton1->Checked)
    {

     // Form1->Image1->Canvas->Draw(0,0,Bitmap);
    //  Display.Videomemory.Draw(Bitmap->Canvas);
    //Form1->Image1->Canvas->Brush->Style = bsClear;
      //Form1->Image1->Canvas->Rectangle(Form1->Image1->Canvas->ClipRect);
      //  Form1->Image1->Canvas->Refresh();
      Display.Videomemory.Draw(Bitmap->Canvas);
      Form1->Image1->Canvas->Draw(0,0,Bitmap);

      // ���������� ���� ������
      Form1->Image1->Canvas->Pen->Width = 3;
      Form1->Image1->Canvas->MoveTo(x0 * mVideomemory_pixel_size,y0 * mVideomemory_pixel_size);

      Form1->Image1->Canvas->LineTo(x0 * mVideomemory_pixel_size,y1 * mVideomemory_pixel_size);
      Form1->Image1->Canvas->LineTo(x1 * mVideomemory_pixel_size,y1 * mVideomemory_pixel_size);
      Form1->Image1->Canvas->LineTo(x1 * mVideomemory_pixel_size,y0 * mVideomemory_pixel_size);
      Form1->Image1->Canvas->LineTo(x0 * mVideomemory_pixel_size,y0 * mVideomemory_pixel_size);

    }
    else
    {

      Bitmap->Canvas->FillRect(TRect(0,0,Bitmap->Width,Bitmap->Height));
      Display.Draw(Bitmap->Canvas);
      Form1->Image1->Canvas->Draw(0,0,Bitmap);

    }

  };

};


#endif

