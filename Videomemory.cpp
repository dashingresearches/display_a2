#include "Videomemory.h"

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/

////////////////////////////////////////////////////////////////////////////////
tVideomemory::tVideomemory(uint arg_bytes_Width, uint arg_Y_size)
{

  bytes_Width = arg_bytes_Width;
  Y_size      = arg_Y_size;

  vector <u1x> Line_2(arg_bytes_Width, 0);

  Memory.resize(arg_Y_size,Line_2);

  Mode = eHalt;

  Wait_flag = false;

  Byte_bitmap_init();

}//tVideomemory::tVideomemory(uint arg_bytes_Width, uint arg_Y_size)


////////////////////////////////////////////////////////////////////////////////
void tVideomemory::Clear()
{

  u1x Fill = 0x1;

  for(int y = 0; y < Memory.size(); y++ )
  {

    for(int x = 0; x < Memory[y].size(); x++ )
    {

      Memory[y][x] = 0;


    }

  }

}//void tVideomemory::Clear()


////////////////////////////////////////////////////////////////////////////////
// �������� �������� ������������� ��������
void tVideomemory::Fill_1()
{

  u1x Fill = 0x1;

  for(int y = 0; y < Memory.size(); y++ )
  {

    for(int x = 0; x < Memory[y].size(); x++ )
    {

      Memory[y][x] = Fill;


    }

    Fill = Fill << 1;

    if(Fill == 0)

      Fill = 0x1;

  }

}//void tVideomemory::Fill_1()


///////////////////////////////////////////////////////////////////////////////////
void tVideomemory::Draw(TCanvas * Canvas)
{

  Canvas->Pen->Color = clBlue;
  Canvas->Pen->Width = 1;

  for(int y = 0; y < Memory.size(); y++ )
  {


  //  if(y&1)

   //   Sleep(1);
      
    for(int x = 0; x < Memory[y].size(); x++ )
    {

      Canvas->Draw( x * 8 * mVideomemory_pixel_size,
                    y * mVideomemory_pixel_size, Byte_bitmap[Memory[y][x]]);

    }//for(int x = 0; x < Memory[y].Size(); x++ )

  }//for(int y = 0; y < Memory.size(); y++ )

}//void tVideomemory::Draw(TCanvas * Canvas)


///////////////////////////////////////////////////////////////////////////////////
void tVideomemory::Byte_bitmap_init()
{

  for (uint i = 0; i < 256; i++)
  {

    Byte_bitmap[i] = new Graphics::TBitmap;
    Byte_bitmap[i]->Width  = 8 * mVideomemory_pixel_size;
    Byte_bitmap[i]->Height = 1 * mVideomemory_pixel_size;

    Byte_bitmap[i]->Canvas->Pen->Color = clBlue;
    Byte_bitmap[i]->Canvas->Pen->Width = 1;

    for(int b = 0, mask = 0x80; b < 8; b ++, mask = mask >> 1)
    {

      if(i & mask)

        Byte_bitmap[i]->Canvas->Brush->Color = clBlue;

      else

        Byte_bitmap[i]->Canvas->Brush->Color = clWhite;

      Byte_bitmap[i]->Canvas->Rectangle( b  * mVideomemory_pixel_size - 1,
                                        -1,
                                        (b + 1) * mVideomemory_pixel_size,
                                        mVideomemory_pixel_size);

    }//for(int b = 0, mask = 0x80; b < 8; b ++, mask = mask >> 1)

  }//for (uint i = 0; i < 256; i++)

}//void tVideomemory::Byte_bitmap_init()
