#ifndef Display_included
#define Display_included

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/


#include "dTypes.h"
using namespace dTypes;

#include <vcl.h>

#include <stdlib.h>
#include <time.h>

#include "Font.h"
#include "Font6x8.h"

#include "Shift_register.h"

#include "Videomemory.h"

#include <algorithm>


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class tDisplay {

  static const u1x Masks_array__left[8];
  static const u1x Masks_array__right[8];
  static const u1x Masks_array__left_for_line_buffer[8];
  static const u1x Masks_array__right_for_line_buffer[8];


  public:

  //////////////////////////////////////////////////////////////////////////////
/*  tDisplay( uint X_size, uint Display_Y_size, uint Line_buffer_Y_size);

  ///////////////////////////////////////////////////////////////////////////////////
  void Out_text (int arg_x0, int arg_y0,
                 int arg_x1, int arg_y1,
                 int arg_x_shift, int arg_y_shift,
                 unsigned char * argText,
                 TMemo * argDebug = 0);   */

  ///////////////////////////////////////////////////////////////////////////////////
  // ���������� ���������� ��������� ������ �� �����
  void Draw(TCanvas * Canvas);

  void Set_font(class tFont* argFont = (tFont*) &Font6x8);

 // private:

  int screenWidth;// = 256,
  int screenHeight;// = 64;
  int screenWidthInBytes;// = (screenWidth + 7)>>3;

  const tFont * Current_font;

  int Font_height;

  vector< vector <u1x> > Line_buffer;

  tVideomemory  Videomemory;

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  void Clear_line_buffer()
  {

    for(int y = 0; y < Font_height; y ++)
    {

      for(int x = 0; x < Line_buffer[y].size(); x ++)
      {

        Line_buffer[y][x] = 0;

      }

    }

  };//void Clear_line_buffer()

  //////////////////////////////////////////////////////////////////////////////
  tDisplay( uint X_size, uint Display_Y_size, uint Line_buffer_Y_size)
  :Videomemory(X_size, Display_Y_size),
   Line_buffer(Line_buffer_Y_size,vector<u1x>((X_size + 7)>>3,0))
  {


    screenWidth = X_size;
    screenHeight = Line_buffer_Y_size;
    screenWidthInBytes = (screenWidth + 7)>>3;

    Current_font = &Font6x8;

  };

  void Out_text(int x0, int y0, int x1, int y1, int x_shift, int y_shift, uchar * text)
  {

    if(x1 < x0)
    {
      int temp = x0;
      x0 = x1;
      x1 = temp;
    }

    if(y1 < y0)
    {
      int temp = y0;
      y0 = y1;
      y1 = temp;
    }

    Font_height = Current_font->Height();


    bool esc_mode = false;

    int screenStartX = x0 + x_shift, screenY = y0 + y_shift;

    int screenX = screenStartX;

    int minX = x0, minY = y0, maxX = x1, maxY = y1;

    if(minX > screenWidth)

      minX = screenWidth;

    if(minX < 0)

      minX = 0;

    if(minY > screenHeight)

      minY = screenHeight;

    if(minY < 0)

      minY = 0;

    bool Buffer_has_data = false;

    Clear_line_buffer();

    while( *text )
    {

      uchar ch = *text;

      if (!esc_mode)
      {

        // ��������� � ��� ��������� ����������� �������� ����� �����
        // ������������� ������������ ������ �������� if
        switch (ch)
        {

          case '\n':

            esc_mode = true;
            continue;

        }

        printToBuffer(screenX, 0, Current_font->Symbol_for(ch), minX, maxX);

        screenX += Current_font->Width_for(ch);

        Buffer_has_data = true;

        text++;

      }//if (!esc_mode)
      else
      {

        switch (ch)
        {

          case '\n':


            copyLineBufferToVideoMemory(screenStartX, screenX, screenY, minX, maxX, minY, maxY);
            Clear_line_buffer();
            Buffer_has_data = false;

            screenX = screenStartX;
            screenY += Font_height;

            esc_mode = false;

            text++;

        }// switch (ch)

      }//else //if (!esc_mode)

    }//while( *text )

    if(Buffer_has_data)

      copyLineBufferToVideoMemory(screenStartX, screenX, screenY, minX, maxX, minY, maxY);

  };

  union{
    u2x Data;
    u1x Array[2];
  }Symbol_buffer;

    void printToBuffer( int lineStartX, int lineStartY, const tFont_item * symbolInfo, int minX, int maxX)
    {

      uchar bytes_Width  = (symbolInfo->Width + 7) >>3;

      for (int y = 0; y < Font_height; y++)
      {

       if(bytes_Width == 1)
      
         (Symbol_buffer.Array[1]) = *((u1x*)(symbolInfo->Image + bytes_Width * y));

       else if (bytes_Width == 2)
         
         (Symbol_buffer.Data) = *((u2x*)(symbolInfo->Image + bytes_Width * y));


        u2x bit_mask = 0x8000;
       
        for (int x = 0; x < symbolInfo->Width; x++)
        {

          bool bit = Symbol_buffer.Data & bit_mask;

          bit_mask = bit_mask >> 1;

          int lineX = lineStartX + x;

          int lineY = lineStartY + y;

          if (lineX >= minX && lineX <= maxX)
          {
            setPixelInBuffer(lineX, lineY, bit);
          }

        }//for (int x = 0; x < symbolInfo->Width; x++)

      }//for (int y = 0; y < Font_height; y++)

    };//void printToBuffer(  int lineStartX, int lineStartY, tFont_item * symbolInfo, int minX, int maxX )

    void setPixelInBuffer(int x, int y, bool bit)
    {

      int bitNumber = (x & 7);
      int byteNumber = x >> 3;

      if (bit)
      {

        Line_buffer[y][byteNumber] |= (0x80 >> bitNumber);

      }

    };//void setPixelInBuffer(int x, int y, bool bit)

    void copyLineBufferToVideoMemory( int screenStartX, int screenEndX, int screenStartY, int minX, int maxX, int minY, int maxY)
    {

      screenStartX = max(screenStartX, minX);
      screenEndX = min(screenEndX, maxX);

      int startByteX = screenStartX >> 3;
      int endByteX = screenEndX >> 3;

      for (int y = 0; y < Font_height; y++)
      {

        int screenY = (screenStartY + y);

        if (! (screenY >= minY && screenY <= maxY)) continue;

        u1x currentVal, mask;

        currentVal = Videomemory[screenY ][startByteX];
        mask = Masks_array__left[screenStartX&0x7];
        currentVal &= mask;
        Line_buffer[y ][ startByteX] |= currentVal;

        currentVal = Videomemory[screenY ][endByteX];
        mask = Masks_array__right[screenEndX&0x7];
        currentVal &= mask;
        Line_buffer[y ][ endByteX] |= currentVal;

        for (int x = startByteX; x <= endByteX; x++)
        {
          Videomemory[screenY ][ x] = Line_buffer[y ][ x];
        }

      }//for (var y = 0; y < lineHeight; y++)

    };//void copyLineBufferToVideoMemory( screenStartX, screenEndX, screenStartY, minX, maxX, minY, maxY)

   
  // ������ ������ ������� �������, ���� ����������� �����������, ������ ������� ������������
  int Text_width(u1x * Text)
  {

    int Sum = 0;

    while(*Text)
    {

      switch(*Text)
      {

        case '\0':
        case '\n':

          return(Sum);

      };

      Sum += Current_font->Width_for(*Text);

      Text++;

    }

    return(Sum);

  }



};// class tDisplay


#endif